import java.util.*;

public class Employee implements Comparable<Employee>{

	private String name;
	private int yrLeavesAllowed;
	private ArrayList<LeaveRecord> leavesTaken;

	@Override
	public int compareTo(Employee another){
		return this.name.compareTo(another.name);
	}
	public Employee(String n, int yLeave)
	{
		this.name = n;
		this.yrLeavesAllowed = yLeave;
		leavesTaken = new ArrayList<>();
	}

	public static void list(ArrayList<Employee> list)
	{
		for (Employee e: list)
		{
			System.out.printf("%s (Entitled Annual Leaves: %d days)\n",e.name,e.yrLeavesAllowed);
		}		
	}
	
	public void addLeaveRecord(LeaveRecord newLvr) throws ExOverlapLeave, ExLeaveMaxedOut, ExSystemDateExcp{
		if(SystemDate.getInstance().getDateInInt() >= newLvr.getEndDay().getDateInInt())
			throw new ExSystemDateExcp();
		
		if(leavesTaken.size() > 0){
			for(LeaveRecord currentlvr: leavesTaken){
				if(newLvr.getStartDay().getDateInInt() < currentlvr.getEndDay().getDateInInt() && currentlvr.getStartDay().getDateInInt() < newLvr.getStartDay().getDateInInt())
					throw new ExOverlapLeave(currentlvr.getStartDay().toString() , currentlvr.getEndDay().toString());
				else if(newLvr.getEndDay().getDateInInt() < currentlvr.getEndDay().getDateInInt() && newLvr.getEndDay().getDateInInt() > currentlvr.getStartDay().getDateInInt())
					throw new ExOverlapLeave(currentlvr.getStartDay().toString() , currentlvr.getEndDay().toString());
			}
		}
		int subtractDays = (newLvr.getEndDay().getDateInInt() - newLvr.getStartDay().getDateInInt()) + 1;
		this.setnewLeaves(subtractDays);
		
		leavesTaken.add(newLvr);
				
		
		Collections.sort(leavesTaken);
	}
	
	public void removeLeaveRecord(LeaveRecord oldLvr){
		int addDays = (oldLvr.getEndDay().getDateInInt() - oldLvr.getStartDay().getDateInInt()) + 1;
		this.setoldLeaves(addDays);
		
		leavesTaken.remove(oldLvr);
		Collections.sort(leavesTaken);
	}
	
	public void listRecords(){
		if ((leavesTaken.size()) > 0)
		{
			for(LeaveRecord lvr: leavesTaken)
			{
				System.out.printf("%s to %s\n", lvr.getStartDay().toString(), lvr.getEndDay().toString());
			}
		}
		else
			System.out.println("No leave record");
	}
	
	public int getRemaningLeaves(){
		return yrLeavesAllowed;
	}
	
	public void setnewLeaves(int subtractDays) throws ExLeaveMaxedOut{
		int temp = yrLeavesAllowed;
		temp -= subtractDays;
		
		if(temp < 0)
			throw new ExLeaveMaxedOut(yrLeavesAllowed);
		yrLeavesAllowed = temp;
	}
	
	public void setoldLeaves(int addDays) {
		yrLeavesAllowed += addDays;
	}
	
	public String getName()
	{
		return name;
	}
	
}
