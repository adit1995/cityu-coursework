
public class ExEmployeeNotFound extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2421906793153390139L;

	public ExEmployeeNotFound(String message){
		super(message);
	}
	
	public ExEmployeeNotFound(){
		super("Employee not found!");
	}

}
