import java.util.ArrayList;
import java.util.Collections;

public class Company {
	private ArrayList<Employee> allEmployees;
	private ArrayList<Team> allTeams;

	private static Company instance = new Company();

	private Company() {
		allEmployees = new ArrayList<>();
		allTeams = new ArrayList<>();
		//allLeaveRecords = new ArrayList<>();
	}

	public static Company getInstance(){
		return instance;
	}

	public Employee createEmployee(String name, int leaveDays) throws ExEmployeeExists, ExAnnLeavesOutOfRange{
		if(leaveDays > 300)
			throw new ExAnnLeavesOutOfRange();
		
		Employee e = null;
		e = new Employee(name, leaveDays); 

		for(Employee existingE: allEmployees){
			if(e.getName().equals(existingE.getName()))
				throw new ExEmployeeExists();
		}
		instance.addEmployee(e);
		return e;
	}

	public Employee searchEmployee(String nameToSearch) throws ExEmployeeNotFound
	{
		for(Employee e: allEmployees)
		{
			if(e.getName().equals(nameToSearch))
				return e;
		}
		throw new ExEmployeeNotFound();
	}
	
	public void addEmployee(Employee e)
	{
		allEmployees.add(e);
		Collections.sort(allEmployees);
	}

	public void removeEmployee(Employee e)
	{
		allEmployees.remove(e);
		Collections.sort(allEmployees);
	}
	
	public void listEmployees(){
		Employee.list(allEmployees);
	}
	
	public LeaveRecord updateEmployeeLeaveRecord(String empName, String startDate, String endDate) throws ExEmployeeNotFound, ExOverlapLeave, ExLeaveMaxedOut, ExSystemDateExcp{
		Employee emp = null;
		emp = searchEmployee(empName);
		
		LeaveRecord newlvr = new LeaveRecord(startDate, endDate);
		
		emp.addLeaveRecord(newlvr);
		
		return newlvr;	
	}
	
	public void addEmployeeLeaveRecord(Employee e, LeaveRecord lvr) throws ExOverlapLeave, ExLeaveMaxedOut, ExSystemDateExcp{
		e.addLeaveRecord(lvr);
	}
	
	public void removeEmployeeLeaveRecord(Employee e, LeaveRecord lvr){
		e.removeLeaveRecord(lvr);
	}
	
	public void listEmployeeLeaves(String employeeName) throws ExEmployeeNotFound{
		Employee e = searchEmployee(employeeName);
		e.listRecords();
	}

	public void listAllLeaves() throws ExEmployeeNotFound{
		for(Employee e: allEmployees){
			System.out.printf("%s:\n",e.getName());
			listEmployeeLeaves(e.getName());
		}
	}

	public Team createTeam(String teamName, String leader) throws ExEmployeeNotFound, ExTeamExists{

		try {
			Employee e = null;
			e = searchEmployee(leader);

			Team t = null;
			t = new Team(teamName,e);

			for(Team existingTeam: allTeams){
				if(t.getName().equals(existingTeam.getName())){
					throw new ExTeamExists();
				}				
			}
			instance.addTeam(t);
			return t;

		} catch (ExEmployeeNotFound e1) {
			throw new ExEmployeeNotFound();
		}
	}

	public void addTeam(Team t)
	{
		allTeams.add(t);
		Collections.sort(allTeams);
	}

	public void removeTeam(Team t)
	{
		allTeams.remove(t);
		Collections.sort(allTeams);
	}
	
	public void listTeams(){
		Team.list(allTeams);
	}
}
