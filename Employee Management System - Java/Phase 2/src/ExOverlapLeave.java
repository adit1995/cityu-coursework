
public class ExOverlapLeave extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2421906793153390139L;

	public ExOverlapLeave(String message){
		super(message);
	}
	
	public ExOverlapLeave(String startDate, String endDate){
		String message = "Overlap with leave from " + startDate + " to " + endDate + "!";
		System.out.println(message);
	}
}
