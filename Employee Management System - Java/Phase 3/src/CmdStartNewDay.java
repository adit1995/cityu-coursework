
public class CmdStartNewDay extends RecordedCommand {
	String prevInstance = SystemDate.getInstance().toString();
	String newInstance;
	
	@Override
	public void execute(String[] cmdParts) {
		SystemDate.getInstance().set(cmdParts[1]);
		newInstance = SystemDate.getInstance().toString();
		
		addUndoCommand(this);
		clearRedoList();
		System.out.println("Done.");
		
	}

	@Override
	public void undoMe() {
		SystemDate.getInstance().set(prevInstance);
		addRedoCommand(this);		
	}

	@Override
	public void redoMe() {
		SystemDate.getInstance().set(newInstance);
		addUndoCommand(this);
	}

}
