
public class ExAnnLeavesOutOfRange extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2421906793153390139L;

	public ExAnnLeavesOutOfRange(String message){
		super(message);
	}
	
	public ExAnnLeavesOutOfRange(){
		super("Annual Leaves out of range (0-300)!");
	}

}
