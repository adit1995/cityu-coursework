
public class CmdAddTeamMember extends RecordedCommand{
	Company company = Company.getInstance();
	Team t;
	Employee e;
	
	public void execute(String[] cmdParts) {
		try {
			if (cmdParts.length != 3)
				throw new ExInsufficientCommandArguments();
			
			e = company.searchEmployee(cmdParts[2]);
			t = company.searchTeam(cmdParts[1]);
			
			company.addTeamMember(t.getName(), e.getName());
			
			addUndoCommand(this);
			clearRedoList();
			System.out.println("Done.");
		} catch (ExInsufficientCommandArguments e) {
			System.out.println(e.getMessage());
		} catch (ExEmployeeNotFound e) {
			System.out.println(e.getMessage());
		} catch (ExTeamNotFound e) {
			System.out.println(e.getMessage());
		} catch (ExEmpExistsInTeam e1) {
			System.out.println(e1.getMessage());
		}
		
	}

	@Override
	public void undoMe() {
		company.removeTeamMember(t, e);
		addRedoCommand(this);
		
	}

	@Override
	public void redoMe() {
		try {
			company.addTeamMember(t.getName(), e.getName());
		} catch (ExEmployeeNotFound | ExTeamNotFound e) {
			System.out.println(e.getMessage());
		} catch (ExEmpExistsInTeam e) {
			System.out.println(e.getMessage());
		}
		addUndoCommand(this);		
	}

}
