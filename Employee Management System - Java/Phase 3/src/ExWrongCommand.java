
public class ExWrongCommand extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2421906793153390139L;

	public ExWrongCommand(String message){
		super(message);
	}
	
	public ExWrongCommand(){
		super("Wrong Command!");
	}

}
