import java.util.*;

public class Team implements Comparable<Team>{
	private String teamName;
	private Employee head;
	private Day dateSetup; //the date when the team was setup
	private ArrayList<Employee> teamMembers;

	public Team(String n, Employee leader)
	{
		this.teamName = n;
		this.head = leader;
		this.dateSetup = SystemDate.getInstance().clone();
		teamMembers = new ArrayList<>();
	}
	
	public static void list(ArrayList<Team> list)
	{
		System.out.printf("%-30s%-10s%-13s\n", "Team Name","Leader","Setup Date");
		for (Team t: list)
		{
			System.out.printf("%-30s%-10s%-13s\n",t.teamName,t.head.getName(),t.dateSetup);
		}		
	}

	public void addMember(Employee e) throws ExEmpExistsInTeam{
		for(Employee emp: teamMembers){
			if(emp.getName().equals(e.getName()))
				throw new ExEmpExistsInTeam();
		}
		teamMembers.add(e);
		e.partOfTeam(this);
		Collections.sort(teamMembers);
	}
	
	public void removeMember(Employee e) {
		teamMembers.remove(e);
		e.noLongerPartOfTeam(this);
		Collections.sort(teamMembers);
	}
	

	@Override
	public int compareTo(Team o) {
		// TODO Auto-generated method stub
		return this.getName().compareTo(o.getName());
	}
	
	public String getName(){
		return teamName;
	}

	public Employee getLeader(){
		return head;
	}

	public void listMembers() {
		for(Employee e: teamMembers){
			System.out.print(e.getName());
			if(this.getLeader().equals(e)){
				System.out.println(" (Head of Team)");
			} else {
				System.out.println("");
			}
			
		}
		
	}
	
}
