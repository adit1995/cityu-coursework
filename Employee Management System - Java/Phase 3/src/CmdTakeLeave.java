
public class CmdTakeLeave extends RecordedCommand{
	Company company = Company.getInstance();
	Employee e;
	LeaveRecord lvr;
	
	@Override
	public void execute(String[] cmdParts) {
		try {
			if (cmdParts.length != 4)
				throw new ExInsufficientCommandArguments();
			
			e = company.searchEmployee(cmdParts[1]);
			lvr = company.updateEmployeeLeaveRecord(cmdParts[1], cmdParts[2], cmdParts[3]);
			
			
			addUndoCommand(this);
			clearRedoList();
			System.out.println("Done.");
		} catch (ExInsufficientCommandArguments e) {
			e.getMessage();
		} catch (ExEmployeeNotFound e) {
			e.getMessage();
		} catch (ExOverlapLeave e1) {
			e1.getMessage();
		} catch (ExLeaveMaxedOut e1) {
			e1.getMessage();
		} catch (ExSystemDateExcp e1) {
			e1.getMessage();
		}
	}

	@Override
	public void undoMe() {
		company.removeEmployeeLeaveRecord(e,lvr);
		addRedoCommand(this);		
	}

	@Override
	public void redoMe() {
		try {
			company.addEmployeeLeaveRecord(e,lvr);
		} catch (ExOverlapLeave e) {
			e.getMessage();
		} catch (ExLeaveMaxedOut e) {
			e.getMessage();
		} catch (ExSystemDateExcp e) {
			e.getMessage();
		}
		addUndoCommand(this);		
	}

}
