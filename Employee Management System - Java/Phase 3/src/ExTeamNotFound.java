
public class ExTeamNotFound extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2421906793153390139L;

	public ExTeamNotFound(String message){
		super(message);
	}
	
	public ExTeamNotFound(){
		super("Team not found!");
	}
}
