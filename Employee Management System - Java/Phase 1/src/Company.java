import java.util.ArrayList;
import java.util.Collections;

public class Company {
	private ArrayList<Employee> allEmployees;
	private ArrayList<Team> allTeams;

	private static Company instance = new Company();

	private Company() {
		allEmployees = new ArrayList<Employee>();
		allTeams = new ArrayList<Team>();
	}

	public static Company getInstance(){
		return instance;
	}

	public void listTeams(){
		Team.list(allTeams);
	}

	public void listEmployees(){
		Employee.list(allEmployees);
	}

	public Employee createEmployee(String name, int leaveDays) throws ExEmployeeExists, ExAnnLeavesOutOfRange{
		if(leaveDays > 300)
			throw new ExAnnLeavesOutOfRange();
		Employee e = null;
		e = new Employee(name, leaveDays); 
		
		for(Employee existingE: allEmployees){
			if(e.getName().equals(existingE.getName()))
				throw new ExEmployeeExists();
		}
		instance.addEmployee(e);
		return e;
	}
	
	public static Employee searchEmployee(ArrayList<Employee> list, String nameToSearch) throws ExEmployeeNotFound
	{
		for(Employee e: list)
		{
			if(e.getName().equals(nameToSearch))
				return e;
		}
		throw new ExEmployeeNotFound();
	}

	public void addEmployee(Employee e)
	{
		allEmployees.add(e);
		Collections.sort(allEmployees);
	}

	public void removeEmployee(Employee e)
	{
		allEmployees.remove(e);
	}

	public Team createTeam(String teamName, String leader) throws ExEmployeeNotFound, ExTeamExists{
		
		try {
			Employee e = null;
			e = searchEmployee(allEmployees, leader);
			
			Team t = null;
			t = new Team(teamName,e);
			
			for(Team existingTeam: allTeams){
				if(t.getName().equals(existingTeam.getName())){
					throw new ExTeamExists();
				}				
			}
			instance.addTeam(t);
			return t;

		} catch (ExEmployeeNotFound e1) {
			throw new ExEmployeeNotFound();
		}
	}

	public void addTeam(Team t)
	{
		allTeams.add(t);
		Collections.sort(allTeams);
	}

	public void removeTeam(Team t)
	{
		allTeams.remove(t);
	}
}
