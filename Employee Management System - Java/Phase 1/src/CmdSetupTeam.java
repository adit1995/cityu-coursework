
public class CmdSetupTeam extends RecordedCommand {
	Company company = Company.getInstance();
	Team t;
	
	@Override
	public void execute(String[] cmdParts) {
		try {
			if (cmdParts.length != 3)
				throw new ExInsufficientCommandArguments();
			
			t = company.createTeam(cmdParts[1], cmdParts[2]);
			
			addUndoCommand(this);
			clearRedoList();
			System.out.println("Done.");
			
		} catch (ExEmployeeNotFound e) {
			System.out.println(e.getMessage());
		} catch (ExTeamExists e) {
			System.out.println(e.getMessage());
		} catch (ExInsufficientCommandArguments e) {
			System.out.println(e.getMessage());
		}
		
	}
	@Override
	public void undoMe() {
		company.removeTeam(t);
		addRedoCommand(this);
		
	}
	@Override
	public void redoMe() {
		company.addTeam(t);
		addUndoCommand(this);
		
	}
	

}
