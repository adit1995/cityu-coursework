
public class ExTeamExists extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2421906793153390139L;

	public ExTeamExists(String message){
		super(message);
	}
	
	public ExTeamExists(){
		super("Team already exists!");
	}

}
