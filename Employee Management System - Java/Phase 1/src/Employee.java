import java.util.*;

public class Employee implements Comparable<Employee>{

	private String name;
	private int yrLeavesAllowed;

	@Override
	public int compareTo(Employee another){
		return this.name.compareTo(another.name);
	}
	public Employee(String n, int yLeave)
	{
		this.name = n;
		this.yrLeavesAllowed = yLeave;
	}

	public static void list(ArrayList<Employee> list)
	{
		for (Employee e: list)
		{
			System.out.printf("%s (Entitled Annual Leaves: %d days)\n",e.name,e.yrLeavesAllowed);
		}		
	}
	
	public String getName()
	{
		return name;
	}

}
