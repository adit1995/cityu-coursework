import java.util.*;

public class Team implements Comparable<Team>{
	private String teamName;
	private Employee head;
	private Day dateSetup; //the date when the team was setup

	public Team(String n, Employee leader)
	{
		this.teamName = n;
		this.head = leader;
		this.dateSetup = SystemDate.getInstance().clone();
	}
	
	public static void list(ArrayList<Team> list)
	{
		System.out.printf("%-30s%-10s%-13s\n", "Team Name","Leader","Setup Date");
		for (Team t: list)
		{
			System.out.printf("%-30s%-10s%-13s\n",t.teamName,t.head.getName(),t.dateSetup);
		}		
	}

	@Override
	public int compareTo(Team o) {
		// TODO Auto-generated method stub
		return this.teamName.compareTo(o.teamName);
	}
	
	public String getName(){
		return teamName;
	}
}
