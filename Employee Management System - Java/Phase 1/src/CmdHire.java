//hire|John|30
public class CmdHire extends RecordedCommand{
	Company company = Company.getInstance();
	Employee e;
	
	@Override
	public void execute(String[] cmdParts){
		try {
			if (cmdParts.length != 3)
				throw new ExInsufficientCommandArguments();
			
			e = company.createEmployee(cmdParts[1], Integer.parseInt(cmdParts[2]));
			
			addUndoCommand(this);
			clearRedoList();
			System.out.println("Done.");
		} catch (NumberFormatException e) {
			System.out.println("Wrong number format!");
		} catch (ExEmployeeExists e) {
			System.out.println(e.getMessage());
		} catch (ExInsufficientCommandArguments e) {
			System.out.println(e.getMessage());
		} catch (ExAnnLeavesOutOfRange e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void undoMe(){
		company.removeEmployee(e);
		addRedoCommand(this);
	}
	
	public void redoMe(){
		company.addEmployee(e);
		addUndoCommand(this);
	}
}
