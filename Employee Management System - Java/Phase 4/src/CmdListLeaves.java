//Eg: listLeaves OR listLeaves|Carol

public class CmdListLeaves implements Command{

	@Override
	public void execute(String[] cmdParts) {
		Company company = Company.getInstance();
		try {
			if(cmdParts.length == 2) //Listing leaves for particular employee
				company.listEmployeeLeaves(cmdParts[1]);
			else if(cmdParts.length == 1) //Listing leaves for every employee
				company.listAllLeaves();
			else
				throw new ExInsufficientCommandArguments();
		} catch (ExEmployeeNotFound e) {
			System.out.println(e.getMessage());
		} catch (ExInsufficientCommandArguments e) {
			System.out.println(e.getMessage());
		}
		
	}
	

}
