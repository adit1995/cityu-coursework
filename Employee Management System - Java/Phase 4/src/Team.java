import java.util.*;

public class Team implements Comparable<Team>{
	private String teamName;
	private Employee head;
	private Day dateSetup; //the date when the team was setup
	private ArrayList<Employee> teamMembers;
	private ArrayList<ActingHead> actingHeadDetails; //Acting Leader details: Employee, Team and Acting Lead Period

	@Override
	public int compareTo(Team o) {
		// TODO Auto-generated method stub
		return this.getName().compareTo(o.getName());
	}
	
	public Team(String n, Employee leader)
	{
		this.teamName = n;
		this.head = leader;
		this.dateSetup = SystemDate.getInstance().clone();
		teamMembers = new ArrayList<>();
		actingHeadDetails = new ArrayList<>();
	}

	public String getName(){
		return teamName;
	}

	public Employee getLeader(){
		return head;
	}

	public void addMember(Employee e) throws ExEmpExistsInTeam{
		for(Employee emp: teamMembers){
			if(emp.getName().equals(e.getName())) //if employee already exists in the team
				throw new ExEmpExistsInTeam();
		}
		teamMembers.add(e);
		e.partOfTeam(this); //The employee is part of the team calling the addMember method
		Collections.sort(teamMembers);
	}
	
	public Employee checkMember(String ename) throws ExEmployeeNotFound{
		for(Employee emp: teamMembers){
			if(emp.getName().equals(ename)){
				return emp;
			}
		}
		throw new ExEmployeeNotFound(ename,this);
	}

	public void removeMember(Employee e) {
		teamMembers.remove(e);
		e.noLongerPartOfTeam(this); //The employee is no longer part of the team calling the removeMember method
		Collections.sort(teamMembers);
	}
	
	//Lists members of the team calling this function
	public void listMembers() {
		for(Employee e: teamMembers){
			System.out.print(e.getName());
			if(this.getLeader().equals(e)){
				System.out.println(" (Head of Team)");
			} else {
				System.out.println("");
			}

		}
		//If the team has any Acting Head for future dates
		if(actingHeadDetails.size() > 0){
			System.out.println("Acting heads:");
			for(ActingHead ach: actingHeadDetails){
				System.out.print(ach.getPeriod().getStartDay() + " to " + ach.getPeriod().getEndDay() +": ");
				System.out.println(ach.getActingHead().getName());
			}
		}

	}

	public void addActingHeadDuty(ActingHead newHead) {
		actingHeadDetails.add(newHead);		
		Collections.sort(actingHeadDetails);
	}
	
	public void checkActingHeadDetails(Employee emp, LeaveRecord newlvr) throws ExCannotTakeLeave {
		for(ActingHead ach: actingHeadDetails){
			if(ach.getActingHead().equals(emp)){
				if(newlvr.getStartDay().getDateInInt() < ach.getPeriod().getEndDay().getDateInInt() && ach.getPeriod().getStartDay().getDateInInt() < newlvr.getStartDay().getDateInInt())
					throw new ExCannotTakeLeave(ach);
				else if(newlvr.getEndDay().getDateInInt() < ach.getPeriod().getEndDay().getDateInInt() && newlvr.getEndDay().getDateInInt() > ach.getPeriod().getStartDay().getDateInInt())
					throw new ExCannotTakeLeave(ach);
			}
		}
		
	}

	public void removeActingHeadDuty(Employee e, LeaveRecord lvr){
		ActingHead _ach = null;
		for(ActingHead ach: actingHeadDetails){
			if(ach.getActingHead().equals(e)){
				if(ach.getPeriod().equals(lvr)){
					_ach = ach;
				}
			}
		}
		actingHeadDetails.remove(_ach);
		Collections.sort(actingHeadDetails);
	}
	
	public static void list(ArrayList<Team> list)
	{
		System.out.printf("%-30s%-10s%-13s\n", "Team Name","Leader","Setup Date");
		for (Team t: list)
		{
			System.out.printf("%-30s%-10s%-13s\n",t.teamName,t.head.getName(),t.dateSetup);
		}		
	}
}
