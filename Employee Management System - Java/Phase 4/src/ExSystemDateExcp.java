
public class ExSystemDateExcp extends Exception {

	private static final long serialVersionUID = 2421906793153390139L;

	public ExSystemDateExcp (String message){
		super(message);
	}
	
	public ExSystemDateExcp() {
		System.out.println("Wrong Date. System date is already " + SystemDate.getInstance().toString() +"!");
	}
}
