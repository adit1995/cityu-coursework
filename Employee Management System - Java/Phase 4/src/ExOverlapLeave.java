
public class ExOverlapLeave extends Exception {

	private static final long serialVersionUID = 2421906793153390139L;
	private static String startDay;
	private static String endDay;

	public ExOverlapLeave(String message){
		super(message);
	}
	
	public ExOverlapLeave(String startDate, String endDate){
		super("Overlap with leave from " + startDate + " to " + endDate + "!");
		startDay = startDate;
		endDay = endDate;
	}
	
	public ExOverlapLeave(Employee e){
		super(e.getName() +" is on leave during " + startDay + " to " + endDay + "!");
	}
}
