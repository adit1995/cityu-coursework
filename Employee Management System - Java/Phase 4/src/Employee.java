import java.util.*;
import java.util.Date;

public class Employee implements Comparable<Employee>{ 

	private String name;
	private int yrLeavesAllowed;
	private ArrayList<LeaveRecord> leavesTaken;
	private ArrayList<String> teamMemberOf;

	@Override
	public int compareTo(Employee another){
		return this.name.compareTo(another.name);
	}
	
	public Employee(String n, int yLeave)
	{
		this.name = n;
		this.yrLeavesAllowed = yLeave;
		leavesTaken = new ArrayList<>();
		teamMemberOf = new ArrayList<>();
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getRemaningLeaves(){
		return yrLeavesAllowed;
	}
	
	public void addLeaveRecord(LeaveRecord newLvr) throws ExOverlapLeave, ExLeaveMaxedOut, ExSystemDateExcp{
		if(SystemDate.getInstance().getDateInInt() >= newLvr.getEndDay().getDateInInt())
			throw new ExSystemDateExcp();
		
		this.checkOverlap(newLvr); //Checks for overlapping leaves
		
		//Using the java.util.Date library as Day's compareTo method isn't sufficient
		long endDay = Date.parse(newLvr.getEndDay().toString());
		long startDay = Date.parse(newLvr.getStartDay().toString());
		int subtractDays = (int) ((endDay - startDay)/(24 * 60 * 60 * 1000)) + 1;

		this.setNewLeaves(subtractDays); //subtracts the number of days from the yrLeavesAllowed field

		leavesTaken.add(newLvr);
		Collections.sort(leavesTaken);
	}
	
	public void checkOverlap(LeaveRecord newLvr) throws ExOverlapLeave{
		if(leavesTaken.size() > 0){
			for(LeaveRecord currentlvr: leavesTaken){
				if(newLvr.getStartDay().getDateInInt() < currentlvr.getEndDay().getDateInInt() && currentlvr.getStartDay().getDateInInt() < newLvr.getStartDay().getDateInInt())
					throw new ExOverlapLeave(currentlvr.getStartDay().toString() , currentlvr.getEndDay().toString());
				else if(newLvr.getEndDay().getDateInInt() < currentlvr.getEndDay().getDateInInt() && newLvr.getEndDay().getDateInInt() > currentlvr.getStartDay().getDateInInt())
					throw new ExOverlapLeave(currentlvr.getStartDay().toString() , currentlvr.getEndDay().toString());
			}
		}
	}
	
	public void setNewLeaves(int subtractDays) throws ExLeaveMaxedOut{
		int temp = yrLeavesAllowed;
		temp -= subtractDays;

		if(temp < 0)
			throw new ExLeaveMaxedOut(yrLeavesAllowed);
		yrLeavesAllowed = temp;
	}

	public void setOldLeaves(int addDays) {
		yrLeavesAllowed += addDays;
	}

	public void removeLeaveRecord(LeaveRecord oldLvr){
		int addDays = (oldLvr.getEndDay().getDateInInt() - oldLvr.getStartDay().getDateInInt()) + 1;
	    this.setOldLeaves(addDays);

		leavesTaken.remove(oldLvr);
		Collections.sort(leavesTaken);
	}
	
	public void partOfTeam(Team t) { //add the employee to the team
		teamMemberOf.add(t.getName());
		Collections.sort(teamMemberOf);

	}
	
	public void noLongerPartOfTeam(Team t) { //removes the employee from the team
		teamMemberOf.remove(t.getName());
		Collections.sort(teamMemberOf);

	}
	
	public void printRoles() throws ExTeamNotFound { //Prints roles of the employee calling the function
		Company company = Company.getInstance();
		if(teamMemberOf.size() == 0)
			System.out.println("No Role");
		else 
		{
			for(String str: teamMemberOf){
				System.out.print(str);
				if(company.searchTeam(str).getLeader().equals(this)){
					System.out.print(" (Head of Team)\n");
				} else {
					System.out.print("\n");
				}
			}
		}
	}
	
	public void listRecords(){ //Print leave records of the employee calling the function
		if ((leavesTaken.size()) > 0)
		{
			for(LeaveRecord lvr: leavesTaken)
			{
				System.out.printf("%s to %s\n", lvr.getStartDay().toString(), lvr.getEndDay().toString());
			}
		}
		else
			System.out.println("No leave record");
	}
	
	public static void list(ArrayList<Employee> list) //Print the name and leaves allowed for the employee calling this function
	{
		for (Employee e: list)
		{
			System.out.printf("%s (Entitled Annual Leaves: %d days)\n",e.name,e.yrLeavesAllowed);
		}		
	}

}
