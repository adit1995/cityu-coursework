//Eg: takeLeave|Bob|3-Jan-2016|9-Jan-2016|Production Team|John|Sales Team|May OR
//	  takeLeave|Bob|3-Jan-2016|4-Jan-2016|Production Team|John OR
// 	  takeLeave|Ann|3-Jan-2016|4-Jan-2016

public class CmdTakeLeave extends RecordedCommand{
	private Company company = Company.getInstance();
	private Employee e, e1, e2;
	private Team t1, t2;
	private LeaveRecord lvr;
	private String[] command;

	@Override
	public void execute(String[] cmdParts) {
		command = cmdParts;
		try {
			if (cmdParts.length != 8 && cmdParts.length != 6 && cmdParts.length != 4)
				throw new ExInsufficientCommandArguments();

			//When the employee taking leave is leader of multiple teams
			if(cmdParts.length == 8){
				t2 = company.searchTeam(cmdParts[6]);
				t1 = company.searchTeam(cmdParts[4]);

				e2 = t2.checkMember(cmdParts[7]);				
				e1 = t1.checkMember(cmdParts[5]);

				e = company.searchEmployee(cmdParts[1]);
				lvr = company.updateEmployeeLeaveRecord(cmdParts);

			}
			//When the employee taking leave is leader of one more team
			else if(cmdParts.length == 6){
				t1 = company.searchTeam(cmdParts[4]);
				e1 = t1.checkMember(cmdParts[5]);

				e = company.searchEmployee(cmdParts[1]);
				lvr = company.updateEmployeeLeaveRecord(cmdParts);
			
			}
			//When the employee taking leave is leader of one more team
			else if(cmdParts.length == 4){
				e = company.searchEmployee(cmdParts[1]);
				lvr = company.updateEmployeeLeaveRecord(cmdParts);
			}
			
			addUndoCommand(this);
			clearRedoList();
			System.out.println("Done.");
			
		} catch (ExInsufficientCommandArguments e) {
			System.out.println(e.getMessage());
		} catch (ExEmployeeNotFound e) {
			System.out.println(e.getMessage());
		} catch (ExOverlapLeave e1) {
			System.out.println(e1.getMessage());
		} catch (ExLeaveMaxedOut e1) {
			e1.getMessage();
		} catch (ExSystemDateExcp e1) {
			e1.getMessage();
		} catch (ExTeamNotFound e3) {
			System.out.println(e3.getMessage());
		} catch (ExSetActingHead e1) {
			System.out.println(e1.getMessage());
		} catch (ExCannotTakeLeave e3) {
			System.out.println(e3.getMessage());
		}
	}

	@Override
	public void undoMe() {
		if(command.length == 8){
			company.removeActingHead(t2, e2,lvr);
			company.removeActingHead(t1, e1, lvr);
		}

		if(command.length == 6){
			company.removeActingHead(t1, e1, lvr);
		}

		company.removeEmployeeLeaveRecord(e,lvr);
		addRedoCommand(this);		
	}

	@Override
	public void redoMe() {
		try {
			company.updateEmployeeLeaveRecord(command);
		} catch (ExOverlapLeave e) {
			System.out.println(e.getMessage());
		} catch (ExLeaveMaxedOut e) {
			e.getMessage();
		} catch (ExSystemDateExcp e) {
			e.getMessage();
		} catch (ExEmployeeNotFound e) {
			System.out.println(e.getMessage());
		} catch (ExSetActingHead e) {
			System.out.println(e.getMessage());
		} catch (ExCannotTakeLeave e) {
			System.out.println(e.getMessage());
		}
		addUndoCommand(this);		
	}

}
