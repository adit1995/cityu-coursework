
public class ActingHead implements Comparable<ActingHead>{
	private Employee actingHead;
	private Team team;
	private LeaveRecord periodOfActingHead;
	
	public ActingHead (Employee e, Team t, LeaveRecord lvr){
		this.actingHead = e;
		this.team = t;
		this.periodOfActingHead = lvr;
	}
	
	public Employee getActingHead(){
		return actingHead;
	}
	
	public Team getTeam(){
		return team;
	}
	
	public LeaveRecord getPeriod(){
		return periodOfActingHead;
	}

	@Override
	public int compareTo(ActingHead another) {
		// TODO Auto-generated method stub
		return this.getPeriod().getStartDay().compareTo(another.getPeriod().getStartDay());
	}
}
