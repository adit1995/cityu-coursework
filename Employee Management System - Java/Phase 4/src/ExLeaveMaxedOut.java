
public class ExLeaveMaxedOut extends Exception {

	private static final long serialVersionUID = 2421906793153390139L;

	public ExLeaveMaxedOut(String message){
		super(message);
	}
	
	public ExLeaveMaxedOut(int yrLeavesAllowed) {
		String message = "Insufficient balance. " + yrLeavesAllowed + " days left only!";
		System.out.println(message);
	}
}
