
public class ExInsufficientCommandArguments extends Exception{

	private static final long serialVersionUID = 2421906793153390139L;

	public ExInsufficientCommandArguments(String message){
		super(message);
	}
	
	public ExInsufficientCommandArguments(){
		super("Insufficient command arguments!");
	}

}
