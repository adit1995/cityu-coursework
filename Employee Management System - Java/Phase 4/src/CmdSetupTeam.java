//Eg: setupTeam|Production Team|Bob

public class CmdSetupTeam extends RecordedCommand {
	private Company company = Company.getInstance();
	private Team t;
	private Employee e;
	
	@Override
	public void execute(String[] cmdParts) {
		try {
			if (cmdParts.length != 3)
				throw new ExInsufficientCommandArguments();
			
			e = company.searchEmployee(cmdParts[2]);
			t = company.createTeam(cmdParts[1], cmdParts[2]);
			
			addUndoCommand(this);
			clearRedoList();
			System.out.println("Done.");
			
		} catch (ExEmployeeNotFound e) {
			System.out.println(e.getMessage());
		} catch (ExTeamExists e) {
			System.out.println(e.getMessage());
		} catch (ExInsufficientCommandArguments e) {
			System.out.println(e.getMessage());
		} catch (ExEmpExistsInTeam e1) {
			System.out.println(e1.getMessage());
		}
		
	}
	@Override
	public void undoMe() {
		company.removeTeam(t);
		addRedoCommand(this);
		
	}
	@Override
	public void redoMe() {
		try {
			company.createTeam(t.getName(), e.getName());
		} catch (ExEmployeeNotFound e) {
			System.out.println(e.getMessage());
		} catch (ExTeamExists e) {
			System.out.println(e.getMessage());
		} catch (ExEmpExistsInTeam e) {
			System.out.println(e.getMessage());
		}
		addUndoCommand(this);
		
	}
	

}
