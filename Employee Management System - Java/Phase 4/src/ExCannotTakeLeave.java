
public class ExCannotTakeLeave extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2421906793153390139L;

	public ExCannotTakeLeave(String message){
		super(message);
	}
	
	public ExCannotTakeLeave(){
		super("Cannot take leave.");
	}

	public ExCannotTakeLeave(ActingHead ach) {
		super("Cannot take leave. " + ach.getActingHead().getName() + " is the acting head of " + ach.getTeam().getName() + " during " + ach.getPeriod().getStartDay().toString() + " to " + ach.getPeriod().getEndDay().toString() + "!");
	}
}
