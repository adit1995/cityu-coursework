//Eg: startNewDay|01-Jan-2016

public class CmdStartNewDay extends RecordedCommand {
	private static String prevInstance = SystemDate.getInstance().toString();
	private static String newInstance;
	
	@Override
	public void execute(String[] cmdParts) {
		SystemDate.getInstance().set(cmdParts[1]);
		newInstance = SystemDate.getInstance().toString();
		
		addUndoCommand(this);
		clearRedoList();
		System.out.println("Done.");
		
	}

	@Override
	public void undoMe() {
		SystemDate.getInstance().set(prevInstance);
		addRedoCommand(this);		
	}

	@Override
	public void redoMe() {
		SystemDate.getInstance().set(newInstance);
		addUndoCommand(this);
	}

}
