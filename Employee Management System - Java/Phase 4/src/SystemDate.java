
public class SystemDate extends Day {
	//Static field for Singleton
	private static SystemDate instance;
	
	//Private constructor for singleton
	private SystemDate(String sDay)	{super(sDay);}
	
	//Static method for getting instance
	public static SystemDate getInstance()
	{
		return instance;
	}
	
	//Public static method for creating singleton
	public static void createTheInstance(String sDay)
	{
		if (instance == null) //only one instance created
			instance = new SystemDate(sDay);
		else
			System.out.println("Tumse na ho paayi");
	}
	
}
