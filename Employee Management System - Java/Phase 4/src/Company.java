import java.util.ArrayList;
import java.util.Collections;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//--------Company Class--------// 
//Every command goes through the Company Class:
//When hiring employee, Company has to create, search for existing employee and add/remove employee
//When setting up team, Company has to create, search for existing team and add/remove team
//When adding team member, Company has to search for existing employee, team and add/remove employee
//When adding/updating leave record, Company has to search for existing employees and add/remove/update leave records
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public class Company {
	private ArrayList<Employee> allEmployees;
	private ArrayList<Team> allTeams;

	private static Company instance = new Company(); //Singleton

	private Company() {
		allEmployees = new ArrayList<>();
		allTeams = new ArrayList<>();
	}

	public static Company getInstance(){
		return instance;
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//--------Employee Operations--------// Company creates, searches for, adds, removes and displays employees
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public Employee createEmployee(String name, int leaveDays) throws ExEmployeeExists, ExAnnLeavesOutOfRange{
		if(leaveDays > 300) 
			throw new ExAnnLeavesOutOfRange();

		Employee e = null;
		e = new Employee(name, leaveDays); 
		
		//Before adding employee to roster, check if employee already exists and throw exception else continue
		for(Employee existingE: allEmployees){
			if(e.getName().equals(existingE.getName()))
				throw new ExEmployeeExists();
		}
		
		addEmployee(e);
		return e;
	}

	public Employee searchEmployee(String nameToSearch) throws ExEmployeeNotFound
	{
		for(Employee e: allEmployees)
		{
			if(e.getName().equals(nameToSearch))
				return e;
		}
		throw new ExEmployeeNotFound();
	}

	public void addEmployee(Employee e)
	{
		allEmployees.add(e);
		Collections.sort(allEmployees);
	}

	public void removeEmployee(Employee e)
	{
		allEmployees.remove(e);
		Collections.sort(allEmployees);
	}

	public void listEmployees(){
		Employee.list(allEmployees);
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//--------Employee Leave Operations--------// Company updates, adds, removes and displays individual and all leave records
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public LeaveRecord updateEmployeeLeaveRecord(String[] cmdParts) throws ExEmployeeNotFound, ExOverlapLeave, ExLeaveMaxedOut, ExSystemDateExcp, ExSetActingHead, ExCannotTakeLeave{
		Employee emp = null;
		emp = searchEmployee(cmdParts[1]);

		LeaveRecord newlvr = new LeaveRecord(cmdParts[2], cmdParts[3]);
		
		Employee newLeader = null;
		
		Team _t = null;

		//When the employee taking leave is leader of multiple teams
		if(cmdParts.length == 8){
			for(Team t: allTeams){
				if(t.getName().equals(cmdParts[6]) && t.getLeader().equals(emp)){
					newLeader = searchEmployee(cmdParts[7]);
					t.checkMember(newLeader.getName()); //To check if the new acting head is the team's member

					try {
						newLeader.checkOverlap(newlvr); //To check if the new acting head has an overlap with the leader's new leave record
					} catch (ExOverlapLeave e) {						
						throw new ExOverlapLeave(newLeader); //Same Exception class, different constructor method
					}
					setActingHead(newLeader, t, newlvr); //Set acting head when all conditions are met
				}

				if(t.getName().equals(cmdParts[4]) && t.getLeader().equals(emp)){
					newLeader = searchEmployee(cmdParts[5]);
					t.checkMember(newLeader.getName());
					
					try {
						newLeader.checkOverlap(newlvr);
					} catch (ExOverlapLeave e) {						
						throw new ExOverlapLeave(newLeader);
					}
					setActingHead(newLeader, t, newlvr);
				}				
			}
			emp.addLeaveRecord(newlvr);
		}
		//When the employee taking leave is leader of one more team
		if(cmdParts.length == 6){
			for(Team t: allTeams){
				if (t.getLeader().equals(emp)) {
					if (t.getName().equals(cmdParts[4]) ) {
						newLeader = searchEmployee(cmdParts[5]);
						t.checkMember(newLeader.getName());
						try {
							newLeader.checkOverlap(newlvr);
						} catch (ExOverlapLeave e) {
							throw new ExOverlapLeave(newLeader);
						}
						_t = t;
					} else {
						throw new ExSetActingHead(t);
					}
					
				} 
				
			}
			setActingHead(newLeader, _t, newlvr);
			emp.addLeaveRecord(newlvr);
		}		
		
		//When an employee takes a leave
		if(cmdParts.length == 4){
			for(Team t: allTeams){
				t.checkActingHeadDetails(emp, newlvr); //To check if the employee in question is an acting head of a team during the requested leave period
			}
			emp.addLeaveRecord(newlvr);
		}
		return newlvr;	
	}


	public void addEmployeeLeaveRecord(Employee e, LeaveRecord lvr) throws ExOverlapLeave, ExLeaveMaxedOut, ExSystemDateExcp{
		e.addLeaveRecord(lvr);
	}

	public void removeEmployeeLeaveRecord(Employee e, LeaveRecord lvr){
		e.removeLeaveRecord(lvr);
	}	
	
	public void listEmployeeLeaves(String employeeName) throws ExEmployeeNotFound{
		Employee e = searchEmployee(employeeName);
		e.listRecords();
	}

	public void listAllLeaves() throws ExEmployeeNotFound{
		for(Employee e: allEmployees){
			System.out.printf("%s:\n",e.getName());
			listEmployeeLeaves(e.getName());
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//--------Team Operations--------//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public Team createTeam(String teamName, String leader) throws ExEmployeeNotFound, ExTeamExists, ExEmpExistsInTeam{

		try {
			Employee e = null;
			e = searchEmployee(leader);

			Team t = null;
			t = new Team(teamName,e);

			for(Team existingTeam: allTeams){
				if(t.getName().equals(existingTeam.getName())){
					throw new ExTeamExists();
				}				
			}
			addTeam(t);
			t.addMember(e);

			return t;
		} catch (ExEmployeeNotFound e1) {
			throw new ExEmployeeNotFound();
		}
	}

	public void addTeam(Team t)
	{
		allTeams.add(t);
		Collections.sort(allTeams);
	}

	public void addTeamMember(String teamName, String empName) throws ExEmployeeNotFound, ExTeamNotFound, ExEmpExistsInTeam{
		Employee e = searchEmployee(empName);
		Team t = searchTeam(teamName);

		t.addMember(e);
	}

	public void removeTeamMember(Team t, Employee e){
		t.removeMember(e);		
	}

	public Team searchTeam(String teamName) throws ExTeamNotFound {
		for(Team t: allTeams){
			if(t.getName().equals(teamName))
				return t;
		}
		throw new ExTeamNotFound();
	}

	public void removeTeam(Team t)
	{
		allTeams.remove(t);
		for(Employee e: allEmployees){
			e.noLongerPartOfTeam(t);
		}
		Collections.sort(allTeams);
	}

	public void setActingHead(Employee e, Team t, LeaveRecord lvr){
		ActingHead newHead = new ActingHead(e, t, lvr);
		t.addActingHeadDuty(newHead);
	}

	public void removeActingHead(Team t, Employee e, LeaveRecord lvr) {
		t.removeActingHeadDuty(e, lvr);
	}

	public void listTeams(){
		Team.list(allTeams);
	}

	public void listRoles(String empName) throws ExEmployeeNotFound, ExTeamNotFound{
		Employee e = searchEmployee(empName);
		e.printRoles();
	}

	public void listTeamMembers(){
		for(Team t: allTeams){
			System.out.println(t.getName() +":");
			t.listMembers();
			System.out.println("");
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
