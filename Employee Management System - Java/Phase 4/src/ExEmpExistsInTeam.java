
public class ExEmpExistsInTeam extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2421906793153390139L;

	public ExEmpExistsInTeam(String message){
		super(message);
	}
	
	public ExEmpExistsInTeam(){
		super("Employee has already joined the team!");
	}
}
