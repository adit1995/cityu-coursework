
public class ExEmployeeExists extends Exception{

	private static final long serialVersionUID = 2421906793153390139L;

	public ExEmployeeExists(String message){
		super(message);
	}
	
	public ExEmployeeExists(){
		super("Employee already exists!");
	}

}
