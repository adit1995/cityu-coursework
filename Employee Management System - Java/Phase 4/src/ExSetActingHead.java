
public class ExSetActingHead extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2421906793153390139L;

	public ExSetActingHead(String message){
		super(message);
	}
	
	public ExSetActingHead(){
		super("Set Acting Head");
	}

	public ExSetActingHead(Team t) {
		super("Please name a member to be the acting head of " + t.getName());
	}
}
