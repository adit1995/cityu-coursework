
public class LeaveRecord implements Comparable<LeaveRecord>{
	private Day startDay;
	private Day endDay;
	
	public LeaveRecord(String startDate, String endDate) {
		this.startDay = new Day(startDate);
		this.endDay = new Day(endDate);
	}

	public Day getStartDay() {
		return startDay;
	}

	public Day getEndDay() {
		return endDay;
	}

	@Override
	public int compareTo(LeaveRecord another) {
		return this.getStartDay().compareTo(another.getStartDay());
	}
}
