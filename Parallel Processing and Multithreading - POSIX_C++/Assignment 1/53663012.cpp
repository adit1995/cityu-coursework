#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <unistd.h>
#include <time.h>

using namespace std;

void *luggageRecord(void *layout) //Function to set the luggage record for the main thread - driver
{
	sleep(rand() % (10 * 60 + 1)); //To simulate passengers arriving within 0-10 minutes

		if (rand() % 101 <= 20) //Assumming 20% of the passengers to have luggage
			*(char *)layout = 'Y';
		else
			*(char *)layout = 'N';
	pthread_exit(NULL);
}


int main(int argc, char* argv[]) //Driver - main thread - with command line runtime arguments
{
	int threadNo = atoi(argv[1]); //Ctsdlib function converting char to integer
	pthread_t passengers[threadNo]; //Creating a tid for the POSIX thread system 
	long row = 0;
	long col = 0;
	char layout[9][4]; //Seating layout of the bus
	int flag;
	int count = 0; //Needed for counting the number of stowed away luggages

	for (row = 0; row < 9; row++)
	{
		for(col = 0; col < 4; col++)
		{
			layout[row][col] = 'E';	//Set seats to be 'Empty'
		}
	}

	srand(time(NULL)); //Seed for random function

	for (row = 0; row < 9; row++)
	{
		for (col = 0; col < 4; col++)
		{
			if (((row * 4) + col) < threadNo)
			{
				flag = pthread_create(&passengers[(row * 4) + col], NULL, luggageRecord, (void *)&layout[row][col]); //Generating user input number of threads
				if (flag)
				{
					cout << "Unable to create thread" << endl;
					exit(-1);
				}
			}
		}
	}

	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if ((i * 4) + j < threadNo)
			{
				flag = pthread_join(passengers[(i * 4) + j], NULL); //Join the worker threads - passengers - to the main thread

					if (flag)
					{
						cout << "Unable to join" << endl;
						exit(-1);
					}
					if (layout[i][j] == 'Y')
					{
						count++; //To count the number of luggages
					}
			}
			cout << layout[i][j]; //Printing the bus layout
		}
		cout << endl;			
	}
	cout << count << " passengers carry luggage" << endl; //Display the count of luggages stowed
	pthread_exit(NULL);
}
