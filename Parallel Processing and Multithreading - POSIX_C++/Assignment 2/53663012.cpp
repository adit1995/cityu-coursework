#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <semaphore.h>

using namespace std;

//Global Baggage Tag Structure
struct Passenger
{
	char hasBag;
	int row, col;
};

Passenger passengerRecord[36]; //Global Queue using an array of type Passenger (the structure)
int queueCounter = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

//Automatic Ticketing Machine implementation
void ticketMachine(long &row, long &col, Passenger &passenger)
{
	if (col == 4)
	{
		col = 0;
		++row;
	}
	else
	{
		sleep(1); //Requirement sleep before being allocated a row number
		passenger.row = row;

		sleep(1);//Requirement sleep before being allocated a column number
		passenger.col = col;
		++col;
	}
}

//Function to check if the passenger has the bag
void baggageCheck(Passenger &passenger)
{
	if (rand() % 101 <= 20) //Assumming 20% of the passengers to have luggage
		passenger.hasBag = 'Y';
	else
		passenger.hasBag = 'N';
}

void enqueue(Passenger &passenger)
{
	passengerRecord[queueCounter] = passenger;
	queueCounter++;
}

//Bus driver dequeuing the Global Queue and acknowledging the baggage tags
void dequeue(char layout[9][4])
{
	for (int row = 0; row < 9; row++)
	{
		for (int col = 0; col < 4; col++)
		{
			if (((row * 4) + col) < queueCounter)
			{
				layout[row][col] = passengerRecord[(row * 4) + col].hasBag;
			}
		}
	}
}

void *mainFunction(void *passenger) //Function to allocate seats to passenger and for the passenger to check for luggage mutually exclusively
{
    sleep(rand() % (2 * 60 + 1)); //To simulate passengers arriving within 0-2 minutes

	long row = 0;
	long col = 0;

	pthread_mutex_lock(&mutex);
		ticketMachine(row, col, *(Passenger*)passenger); //automatic ticket machine allocating seats
	pthread_mutex_unlock(&mutex);

	sleep(rand() % (60 + 1)); //Random wait before the passenger gets on the bus
	baggageCheck(*(Passenger*)passenger);

	pthread_mutex_lock(&mutex);
		enqueue(*(Passenger*)passenger); //inserting the passenger in the queue
	pthread_mutex_unlock(&mutex);

	pthread_exit(NULL);
}




int main(int argc, char* argv[]) //Driver - main thread - with command line runtime arguments
{
	int threadNo = atoi(argv[1]); //Ctsdlib function converting char to integer

	pthread_t passengers[threadNo]; //Creating a tid for the POSIX thread system

	Passenger passenger[threadNo];

	long row = 0;
	long col = 0;

	char layout[9][4]; //Seating layout of the bus

	int flag;

	int count = 0; //Needed for counting the number of stowed away luggages

	for (row = 0; row < 9; row++)
	{
		for(col = 0; col < 4; col++)
		{
			layout[row][col] = 'E';	//Set seats to be 'Empty'
		}
	}

	srand(time(NULL)); //Seed for random function

	for (row = 0; row < 9; row++)
	{
		for (col = 0; col < 4; col++)
		{
			if (((row * 4) + col) < threadNo)
			{
				flag = pthread_create(&passengers[(row * 4) + col], NULL, mainFunction, (void *)&passenger[(row*4) + col]); //Generating user input number of threads and passing
				if (flag)                                                                                                   //one passenger to each thread for manipulation
				{
					cout << "Unable to create thread" << endl;
					exit(-1);
				}
			}
		}
	}

	for (int i = 0; i < threadNo; i++)
	{
		flag = pthread_join(passengers[i], NULL);
		if (flag)
		{
			cout << "Error: unable to join threads, " << flag << endl;
			exit(-1);
		}
	}

	for (int i = 0; i < threadNo; i++)
	{
		dequeue(layout);
	}

	for (int i = 0; i < 9; i++){
		for (int j = 0; j < 4; j++)
		{
			if (layout[i][j] == 'Y')
				count++;
			cout << layout[i][j];
		}
		cout << endl;
	}
	cout << count << " passengers carry luggage" << endl; //Display the count of luggages stowed
	pthread_exit(NULL);
}
