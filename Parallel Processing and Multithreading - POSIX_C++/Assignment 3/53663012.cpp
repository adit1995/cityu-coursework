#include <iostream>
#include <cstdlib>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <semaphore.h>

using namespace std;

//Global Baggage Tag Structure
struct Passenger
{
	char hasBag;
	int row, col;
};

Passenger passengerRecord[36]; //Global Queue using an array of type Passenger (the structure)
int queueCounter = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

sem_t seatsFull;
sem_t busStart;

char layout[9][4]; //Seating layout of the bus

int totalPassengers, defaultPassengers;
long row = 0;
long col = 0;

//Automatic Ticketing Machine implementation
void ticketMachine(Passenger &passenger)
{
	if (row == 9 && col == 4){
		sleep(1); //Requirement sleep before being allocated a row number
		passenger.row = row;

		sleep(1);//Requirement sleep before being allocated a column number
		passenger.col = col;
		row = 0;
		col = 0;
	}

	if (col == 4)
	{
		col = 0;
		passenger.col = col;
		++row;
		passenger.row = row;
	}
	else
	{
		sleep(1); //Requirement sleep before being allocated a row number
		passenger.row = row;

		sleep(1);//Requirement sleep before being allocated a column number
		passenger.col = col;
		++col;
	}
}

//Function to check if the passenger has the bag
void baggageCheck(Passenger &passenger)
{
	if (rand() % 101 <= 20) //Assumming 20% of the passengers to have luggage
		passenger.hasBag = 'Y';
	else
		passenger.hasBag = 'N';
}

void enqueue(Passenger &passenger)
{
	passengerRecord[queueCounter] = passenger;
	queueCounter++;
}

//Bus driver dequeuing the Global Queue and acknowledging the baggage tags
void dequeue(char layout[9][4])
{
	for (int row = 0; row < 9; row++)
	{
		for (int col = 0; col < 4; col++)
		{
			if (((row * 4) + col) < queueCounter)
			{
				layout[row][col] = passengerRecord[(row * 4) + col].hasBag;
			}
		}
	}
}

void *mainFunction(void *passenger) //Function to allocate seats to passenger and for the passenger to check for luggage mutually exclusively
{
    sleep(rand() % (2 * 60 + 1)); //To simulate passengers arriving within 0-2 minutes

	Passenger p = *(Passenger*)passenger;

	pthread_mutex_lock(&mutex);
		ticketMachine(p); //automatic ticket machine allocating seats
	pthread_mutex_unlock(&mutex);

	sem_wait(&seatsFull);
	sleep(rand() % (60 + 1)); //Random wait before the passenger gets on the bus
	baggageCheck(p);

	pthread_mutex_lock(&mutex);
		enqueue(p); //inserting the passenger in the queue
	pthread_mutex_unlock(&mutex);

	pthread_mutex_lock(&mutex);
		totalPassengers--;
	pthread_mutex_unlock(&mutex);

	if (queueCounter == 36)
	{
		pthread_mutex_lock(&mutex);
			sem_post(&busStart);
		pthread_mutex_unlock(&mutex);
			
	}

	pthread_exit(NULL);
}


void *driverFunction(void *number){
	int *integer = (int *)number;

	double arriveTime = 0;
	double leaveTime = 0;

	time_t reachTime; time_t transitTime;

	time(&reachTime);

	while ((totalPassengers) > 0){
		for (int i = 0; i < 36; i++)
		{
			sem_post(&seatsFull);
		}

		sem_wait(&busStart);

		time(&transitTime);

		leaveTime = arriveTime + difftime(transitTime, reachTime);

		cout << "Coach arrives at time " << arriveTime << endl;
		cout << "Coach departs at time " << leaveTime << endl;

		for (int i = 0; i < 36; i++){
			dequeue(layout);
		}
		

		int count = 0; //Needed for counting the number of stowed away luggages

		for (int i = 0; i < 9; i++){
			for (int j = 0; j < 4; j++)
			{
				if (layout[i][j] == 'Y')
					count++;
				cout << layout[i][j];
			}
			cout << endl;
		}
		cout << count << " passengers carry luggage" << endl; //Display the count of luggages stowed

		sleep(60 * 10);

		for (row = 0; row < 9; row++)
		{
			for (col = 0; col < 4; col++)
			{
				layout[row][col] = 'E';	//Set seats to be 'Empty'
			}
		}

		queueCounter = 0;

		time(&reachTime);
		arriveTime = leaveTime + difftime(reachTime, transitTime);
	}

	pthread_exit(NULL);
}

int main(int argc, char* argv[]) //Driver - main thread - with command line runtime arguments
{
	int threadNo = atoi(argv[1]); //Ctsdlib function converting char to integer
	totalPassengers = threadNo;
	defaultPassengers = threadNo;

	pthread_t passengers[threadNo]; //Creating a tid for the POSIX thread system

	Passenger passenger[threadNo];

	int row = 0;
	int col = 0;

	

	int flag;

	sem_init(&busStart, 0, 0);
	sem_init(&seatsFull, 0, 0);

	for (row = 0; row < 9; row++)
	{
		for(col = 0; col < 4; col++)
		{
			layout[row][col] = 'E';	//Set seats to be 'Empty'
		}
	}

	srand(time(NULL)); //Seed for random function

	//driver thread
	pthread_t coachDriver;
	flag = pthread_create(&coachDriver, NULL, driverFunction, (void *)&threadNo);
	if (flag)                                                                                                   //one passenger to each thread for manipulation
	{
		cout << "Unable to create thread" << endl;
		exit(-1);
	}

	//passenger threads
	for (row = 0; row < 9; row++)
	{
		for (col = 0; col < 4; col++)
		{
			if (((row * 4) + col) < threadNo)
			{
				flag = pthread_create(&passengers[(row * 4) + col], NULL, mainFunction, (void *)&passenger[(row*4) + col]); //Generating user input number of threads and passing
				if (flag)                                                                                                   //one passenger to each thread for manipulation
				{
					cout << "Unable to create thread" << endl;
					exit(-1);
				}
			}
		}
	}

	//joining driver
	flag = pthread_join(coachDriver, NULL);
	if (flag)
	{
		cout << "Error: unable to join threads, " << flag << endl;
		exit(-1);
	}

	//joining passengers
	for (int i = 0; i < threadNo; i++)
	{
		flag = pthread_join(passengers[i], NULL);
		if (flag)
		{
			cout << "Error: unable to join threads, " << flag << endl;
			exit(-1);
		}
	}

	sem_destroy(&busStart);
	sem_destroy(&seatsFull);

	
	pthread_exit(NULL);
}
