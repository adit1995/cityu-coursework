var formData = {
	"movieObject":
	[
		{
			"movieTitle": "The Martian",
			"movieDate": 30,
			"movieTimeAndHouse": [
					{
						"time": 11,
						"house": 1
					},
					{
						"time": 13,
						"house": 3
					},
					{
						"time": 15,
						"house": 2
					}
			]
		},
		//----------------------------------------------
		{
			"movieTitle": "Interstellar",
			"movieDate": 30,
			"movieTimeAndHouse": [
					{
						"time": 11,
						"house": 3
					},
					{
						"time": 13,
						"house": 1
					},
					{
						"time": 16,
						"house": 2
					}
			]
		},
		//--------------------------------------------
		{
			"movieTitle": "Armaggedon",
			"movieDate": 30,
			"movieTimeAndHouse": [
					{
						"time": 11,
						"house": 2
					},
					{
						"time": 15,
						"house": 3
					},
					{
						"time": 15,
						"house": 1
					}
			]
		}
	]
};