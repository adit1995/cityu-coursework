var seatingPlan = {
	"movieObject":
	[
		{
			"movieTitle": "The Martian",
			"movieDate": 30,
			"movieTimeAndHouse": [
					{
						"time": 11,
						"house": 1,
						"seats": [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
					},
					{
						"time": 13,
						"house": 3,
						"seats": [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
					},
					{
						"time": 15,
						"house": 2,
						"seats": [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
					}
			]
		},
		//----------------------------------------------
		{
			"movieTitle": "Interstellar",
			"movieDate": 30,
			"movieTimeAndHouse": [
					{
						"time": 11,
						"house": 3,
						"seats": [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
					},
					{
						"time": 13,
						"house": 1,
						"seats": [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
					},
					{
						"time": 16,
						"house": 2,
						"seats": [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
					}
			]
		},
		//--------------------------------------------
		{
			"movieTitle": "Armaggedon",
			"movieDate": 30,
			"movieTimeAndHouse": [
					{
						"time": 11,
						"house": 2,
						"seats": [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
					},
					{
						"time": 15,
						"house": 3,
						"seats": [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
					},
					{
						"time": 15,
						"house": 1,
						"seats": [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
					}
			]
		}
	]
};