#include<iostream> 
#include<fstream>   
using namespace std;

char getASCII(double a, double b, double c){
	double gray_level = 0;
	double max_GrayLevel = 0.3*(255.0 / 256.0) + 0.6*(255.0 / 256.0) + 0.11*(255.0 / 256.0);
	gray_level = (0.3*a/256 + 0.6*b/256 + 0.11*c/256);
	if (gray_level > 0.6)
	{
		int toInteger1 = 65 + (gray_level - 0.6)*(26 / (max_GrayLevel - 0.6));
		return (char) toInteger1;
	}
	else if (gray_level >= 0.3 && gray_level <= 0.6)
	{
		int toInteger2 = 97 + (gray_level - 0.3)*(26 / 0.3);
		return (char) toInteger2;
	}
	else if (gray_level < 0.3)
	{
		//gray_level = (gray_level - 0.06) * 65 + 65;
		int toInteger3 = 32 + (gray_level * (16 / 0.3));
		return (char) toInteger3;
	}
	return '0';
};

void main()
{
	// Declare Bitmap File Header Variable
	int rgb;
	unsigned short signature;
	unsigned int file_size;
	unsigned int reserve;
	unsigned int offset;

	// Declare Bitmap Info Header Variable
	unsigned int header_size;
	int width;
	int height;
	unsigned short planes;
	unsigned short bits_per_pixel;
	unsigned int compression;
	unsigned int data_size;
	unsigned int horizontal_res;
	unsigned int vertical_res;
	unsigned int num_color;
	unsigned int imp_color;

	ifstream fin; // find image input

	char filename[50];
	cin >> filename;
	fin.open(filename, ios::binary);

	while (fin.is_open() == false)
	{
		cin >> filename;
		fin.open(filename, ios::binary);
	}

	// read file header 
	fin.read((char *)&signature, 2); // read signature

	fin.read((char *)&file_size, 4); // read fileSize; 
	fin.read((char *)&reserve, 4); // read reserve; 
	fin.read((char *)&offset, 4); // read offset; 

	// Read info header 
	fin.read((char *)&header_size, 4);
	if (header_size == 40)
	{
		fin.read((char *)&width, 4);
		fin.read((char *)&height, 4);
		fin.read((char *)&planes, 2);
		fin.read((char *)&bits_per_pixel, 2);
		fin.read((char *)&compression, 4);
		fin.read((char *)&data_size, 4);
		fin.read((char *)&horizontal_res, 4);
		fin.read((char *)&vertical_res, 4);
		fin.read((char *)&num_color, 4);
		fin.read((char *)&imp_color, 4);
	}

	else if (header_size == 12)
	{
		fin.read((char *)&width, 2);
		fin.read((char *)&height, 2);
		fin.read((char *)&planes, 2);
		fin.read((char *)&bits_per_pixel, 2);
		fin.read((char *)&compression, 4);
		fin.read((char *)&data_size, 4);
		fin.read((char *)&horizontal_res, 4);
		fin.read((char *)&vertical_res, 4);
		fin.read((char *)&num_color, 4);
		fin.read((char *)&imp_color, 4);
	}

	int padding = (width * 3 + 3) / 4 * 4;

	unsigned char* pixelated = new unsigned char[padding];
	unsigned char temp_store;

	char ASCII[1000][1000];
	int i = 0;
	int j = 0;

	for (i = 0; i < height; i++)
	{
		fin.read((char*)pixelated, padding);
		for (j = 0; j < width * 3; j += 3)
		{
			// Conversion
			temp_store = pixelated[j];
			pixelated[j] = pixelated[j + 2];
			pixelated[j + 2] = temp_store;

			ASCII[i][j] = getASCII((int)pixelated[j], (int)pixelated[j + 1], (int)pixelated[j + 2]);
		}
	}
	fin.close();

	for (i = height - 1; i >= 0; i--){
		for (j = 0; j<width * 3; j += 3){
			if (ASCII[i][j] == '[')
				cout << "Z";
			else
				cout << ASCII[i][j];
		}
		cout << endl;
	}
}
