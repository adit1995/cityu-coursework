#include <iostream>
#include <string>

using namespace std;


void enterText()
{
	int i = 0, j = 0, k = 0;
	char card[50][70] = { 0 };
	int maxHeight = 50;
	int maxWidth = 70;
	int lName, lMessage;
	string name, message;
	//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
		//Taking inputs from user: Name and Greeting Message|
	//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\


	cout << "Enter Name: ";
	getline(cin, name);
	lName = name.length();

	cout << "Enter Message: ";
	getline(cin, message);
	lMessage = message.length();
}

void snow()
{
	int i = 0, j = 0, k = 0;
	char card[50][70] = { 0 };
	int maxHeight = 50;
	int maxWidth = 70;
	int lName, lMessage;
	string name, message;
	//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
		//Full body background ASCII Art from Scratch - Snow|
	//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\


	for (i = 0; i < (maxHeight - 1); i++)
	{
		for (j = 0; j < (maxWidth - 1); j++)
		{
			if ((i % 2 == 0) && (j % 3 == 0))
			{
				card[i][j] = 'O';
				card[i + 1][j + 1] = 'o';
			}
		}
	}

	//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
		//Sectional removal of background snow|
	//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\

	//Background of text section
	for (i = 20; i < 30; i++)
	{
		for (j = 20; j < 50; j++)
		{
			card[i][j] = char(32);
		}
	}

	//Background of Upper section
	for (i = 1; i < 13; i++)
	{
		for (j = 1; j < 69; j++)
		{
			card[i][j] = char(32);
		}
	}

	//Background of Lower section
	for (i = 38; i < 49; i++)
	{
		for (j = 1; j < 69; j++)
		{
			card[i][j] = char(32);
		}
	}
}

void border()
{
	int i = 0, j = 0, k = 0;
	char card[50][70] = { 0 };
	int maxHeight = 50;
	int maxWidth = 70;
	int lName, lMessage;
	string name, message;
	//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
		//Rendering the borders for the card|
	//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\

	//Horizontal Borders
	for (i = 0; i < maxHeight; i++)
	{
		for (j = 0; j < maxWidth; j++)
		{
			if (i == 0 || i == 49)
			{
				card[i][j] = char(45);
			}
		}
	}

	//Verticle Borders
	for (i = 1; i < (maxHeight - 1); i++)
	{
		for (j = 0; j < maxWidth; j++)
		{
			if (j == 0 || j == 69)
				card[i][j] = char(124);
		}
	}
}

void insertText()
{
	int i = 0, j = 0, k = 0;
	char card[50][70] = { 0 };
	int maxHeight = 50;
	int maxWidth = 70;
	int lName, lMessage;
	string name, message;
	//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
		//Rendering default text and User input text|
	//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\

	//'DEAR'
	for (j = 33, i = 0; j < 37; j++, i++)
	{
		string Default = "DEAR";
		card[21][j] = Default[i];
	}


	//User-input Target
	for (j = 21, i = 0; j < (k = 21 + lName); j++, i++)
	{
		if (lName % 2 == 0 || lName % 2 == 1)
		{
			int ifEven = (28 - lName) / 2;
			card[22][j + ifEven] = name[i];
		}
	}

	//'MERRY CHRISTMAS!'
	for (j = 27, i = 0; j < 43; j++, i++)
	{
		string Default = "MERRY CHRISTMAS!";
		card[24][j] = Default[i];
	}

	//User-input Greeting Message
	for (j = 21, i = 0; j < (k = 21 + lMessage); j++, i++)
	{
		if (lMessage % 2 == 0 || lMessage % 2 == 1)
		{
			int ifEven = (28 - lMessage) / 2;
			card[26][j + ifEven] = message[i];
		}
	}
}

void generateArt()
{
	int i = 0, j = 0, k = 0;
	char card[50][70] = { 0 };
	int maxHeight = 50;
	int maxWidth = 70;
	int lName, lMessage;
	string name, message;
	//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/\/
	//Generating ASCII Art from Scratch - 3D (Right Skew) Gift Box, Christmas Tree (With Decorations)|
	//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

	//3D (Right Skew) Gift Box
	char giftBox[11][16] = {};

	for (i = 0; i < 11; i++)
	{
		for (j = 0; j < 16; j++)
		{
			if (i == 2 && (j>4 && j < 14))
			{
				giftBox[i][j] = '_';
			}

			if (i == 3)
			{
				giftBox[i][j] = char(32);

				if (j == 3 || j == 7 || j == 9 || j == 13)
					giftBox[i][j] = '/';

				if (j == 4 || j == 5 || j == 6 || j == 10 || j == 11 || j == 12)
					giftBox[i][j] = '_';

				if (j == 14)
					giftBox[i][j] = '|';
			}

			if (i == 4)
			{
				giftBox[i][j] = char(32);

				if (j == 2 || j == 12)
					giftBox[i][j] = '/';

				if (j == 3 || j == 4 || j == 5 || j == 9 || j == 10 || j == 11)
					giftBox[i][j] = '_';

				if (j == 13 || j == 14)
					giftBox[i][j] = '|';
			}

			if (i == 5)
			{
				giftBox[i][j] = char(32);

				if (j == 1 || j == 5 || j == 7 || j == 11)
					giftBox[i][j] = '/';

				if (j == 2 || j == 3 || j == 4 || j == 6 || j == 8 || j == 9 || j == 10)
					giftBox[i][j] = '_';

				if (j == 12 || j == 13 || j == 14)
					giftBox[i][j] = '|';
			}

			if (i == 6)
			{
				giftBox[i][j] = char(32);

				if (j == 13)
					giftBox[i][j] = '/';

				if (j == 1 || j == 5 || j == 7 || j == 11 || j == 12 || j == 14)
					giftBox[i][j] = '|';
			}

			if (i == 7)
			{
				giftBox[i][j] = char(32);

				if (j == 12 || j == 13)
					giftBox[i][j] = '/';

				if (j == 2 || j == 3 || j == 4 || j == 8 || j == 9 || j == 10)
					giftBox[i][j] = '_';

				if (j == 1 || j == 5 || j == 7 || j == 11 || j == 14)
					giftBox[i][j] = '|';
			}

			if (i == 8)
			{
				giftBox[i][j] = char(32);

				if (j == 12 || j == 14)
					giftBox[i][j] = '/';

				if (j == 2 || j == 3 || j == 4 || j == 8 || j == 9 || j == 10)
					giftBox[i][j] = '_';

				if (j == 1 || j == 11 || j == 13)
					giftBox[i][j] = '|';
			}

			if (i == 9)
			{
				giftBox[i][j] = char(32);

				if (j == 13)
					giftBox[i][j] = '/';

				if (j == 1 || j == 5 || j == 7 || j == 11 || j == 12)
					giftBox[i][j] = '|';
			}

			if (i == 10)
			{
				giftBox[i][j] = char(32);

				if (j == 12)
					giftBox[i][j] = '/';

				if (j == 2 || j == 3 || j == 4 || j == 6 || j == 8 || j == 9 || j == 10)
					giftBox[i][j] = '_';

				if (j == 1 || j == 5 || j == 7 || j == 11)
					giftBox[i][j] = '|';
			}
		}
	}

	//Christmas Tree (With Decorations)
	char tree[11][16] = {};

	for (i = 0; i < 11; i++)
	{
		for (j = 0; j < 16; j++)
		{
			if (i == 1)
			{
				if (j == 7)
					tree[i][j] = '/';

				if (j == 8)
					tree[i][j] = '\\';
			}

			if (i == 2)
			{
				if (j == 6)
					tree[i][j] = '/';

				if (j == 9)
					tree[i][j] = '\\';

				if (j == 7 || j == 8)
					tree[i][j] = '#';
			}

			if (i == 3)
			{
				if (j == 5)
					tree[i][j] = '/';

				if (j == 10)
					tree[i][j] = '\\';

				if (j == 6 || j == 7 || j == 8 || j == 9)
					tree[i][j] = '0';
			}

			if (i == 4)
			{
				if (j == 4)
					tree[i][j] = '/';

				if (j == 11)
					tree[i][j] = '\\';

				if (j == 5 || j == 6 || j == 7 || j == 8 || j == 9 || j == 10)
					tree[i][j] = '#';
			}

			if (i == 5)
			{
				if (j == 3)
					tree[i][j] = '/';

				if (j == 12)
					tree[i][j] = '\\';

				if (j == 4 || j == 5 || j == 6 || j == 7 || j == 8 || j == 9 || j == 10 || j == 11)
					tree[i][j] = '0';
			}

			if (i == 6)
			{
				if (j == 2)
					tree[i][j] = '/';

				if (j == 13)
					tree[i][j] = '\\';

				if (j == 3 || j == 4 || j == 5 || j == 6 || j == 7 || j == 8 || j == 9 || j == 10 || j == 11 || j == 12)
					tree[i][j] = '#';
			}

			if (i == 7)
			{
				if (j == 1)
					tree[i][j] = '/';

				if (j == 14)
					tree[i][j] = '\\';

				if (j == 2 || j == 3 || j == 4 || j == 5 || j == 6 || j == 7 || j == 8 || j == 9 || j == 10 || j == 11 || j == 12 || j == 13)
					tree[i][j] = '0';
			}

			if (i == 8)
			{
				if (j == 0 || j == 2 || j == 4 || j == 10 || j == 12 || j == 14)
					tree[i][j] = '/';

				if (j == 1 || j == 3 || j == 5 || j == 11 || j == 13 || j == 15)
					tree[i][j] = '\\';

				if (j == 6 || j == 7 || j == 8 || j == 9)
					tree[i][j] = '|';
			}

			if (i == 9)
			{
				if (j == 6 || j == 7 || j == 8 || j == 9)
					tree[i][j] = '|';
			}
		}
	}
}

void insertUpper()
{
	int i = 0, j = 0, k = 0;
	char card[50][70] = { 0 };
	int maxHeight = 50;
	int maxWidth = 70;
	int lName, lMessage;
	string name, message;
	char tree[11][16];
	char giftBox[11][16];
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/
//Inserting 3D (Right Skew) Gift Box, Christmas Tree (With Decorations) into the Christmas Card|
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

//Upper Layer
for (i = 1; i < 12; i++)
{
for (j = 1; j < 17; j++)
{
card[i][j] = giftBox[i - 1][j - 1];
}

for (j = 18; j < 34; j++)
{
card[i][j] = tree[i - 1][j - 18];
}

for (j = 35; j < 51; j++)
{
card[i][j] = giftBox[i - 1][j - 35];
}

for (j = 52; j < 68; j++)
{
card[i][j] = tree[i - 1][j - 52];
}
}
}

void insertLower()
{
	int i = 0, j = 0, k = 0;
	char card[50][70] = { 0 };
	int maxHeight = 50;
	int maxWidth = 70;
	int lName, lMessage;
	string name, message;
	char tree[11][16];
	char giftBox[11][16];
	//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\//\/\/\/\/\/\/\/\/\/\/
	//Inserting 3D (Right Skew) Gift Box, Christmas Tree (With Decorations) into the Christmas Card|
	//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
	//Lower layer
	for (i = 38; i < 49; i++)
	{
		for (j = 1; j < 17; j++)
		{
			card[i][j] = tree[i - 38][j - 1];
		}

		for (j = 18; j < 34; j++)
		{
			card[i][j] = giftBox[i - 38][j - 18];
		}

		for (j = 35; j < 51; j++)
		{
			card[i][j] = tree[i - 38][j - 35];
		}

		for (j = 52; j < 68; j++)
		{
			card[i][j] = giftBox[i - 38][j - 52];
		}
	}
}

void renderCard()
{
	int i = 0, j = 0, k = 0;
	char card[50][70] = { 0 };
	int maxHeight = 50;
	int maxWidth = 70;
	int lName, lMessage;
	string name, message;
	//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
		//Render the Christmas Card with Text, ASCII Art and Borders|
	//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\

	for (i = 0; i < maxHeight; i++)
	{
		for (j = 0; j < maxWidth; j++)
		{
			if (j % 70 == 0)
				cout << endl;
			cout << card[i][j];
		}
	}
	cout << "\n";
}

void main()
{
	int i = 0, j = 0, k = 0;
	char card[50][70] = { 0 };
	int maxHeight = 50;
	int maxWidth = 70;
	int lName, lMessage;
	string name, message;

	cout << "Aditya Kedia" << " [53663012]\n" << endl;

	enterText();
	snow();
	border();
	insertText();
	generateArt();

	insertUpper();
	insertLower();

	renderCard();

}
